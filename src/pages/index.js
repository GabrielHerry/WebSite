import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>"Un petit mot de Gabriel ..."</h1>
    <h2>"Pour mettre en pratique ce que j'ai appris aujourd'hui ..."</h2>
    <h3>"Je t'aime Marion"</h3>
    <Link to="/page-2/">aller à la page 2</Link>
  </Layout>
)

export default IndexPage
