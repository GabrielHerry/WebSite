import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const SecondPage = () => (
  <Layout>
    <SEO title="Marion <3" />
    <h1>"J'ai hâte de te retrouver :)"</h1>
    <Link to="/">Retourner à la page principale :)</Link>
  </Layout>
)

export default SecondPage
